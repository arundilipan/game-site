var express = require('express'),
		doT = require('dot'),
    stylus = require('stylus'),
    pogo = require('pogo');
var app = express.createServer();

app.configure(function(){
  app.use(express.methodOverride());
  app.use(express.bodyParser());
  app.use(app.router);
  app.use(stylus.middleware({src: __dirname + '/public/css/'}));
  app.set('view engine', 'html');
  app.register('.html', doT);
  app.set('views', __dirname + '/public');
  app.set('view options', {layout: false});
  app.set('basepath',__dirname + '/public');
});

app.configure('development', function(){
  app.use(express.static(__dirname + '/public'));
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  var oneYear = 31557600000;
  app.use(express.static(__dirname + '/public', { maxAge: oneYear }));
  app.use(express.errorHandler());
});

console.log("Web server has started.\nPlease log on http://127.0.0.1:3001/index.html");

//ROUTES
app.get('/', function(req, res) {
  var templateData = {};
  res.render('index', templateData);
});

/* GAMES */
app.get('/games/brickle', function(req, res) {
  
});

app.get('/games/huzzah', function(req, res) {

});
/* END GAMES */


//END ROUTES

app.listen(3001);
